provider "aws" {
  profile = "default"
  region = "us-east-2"
}
resource "aws_default_vpc" "eb_v2_vpc" {
}

resource "aws_default_subnet" "eb_v2_subnet" {
  availability_zone = "us-east-2a"
  #cidr_block = "0.0.0.0/0"
  tags = {
    "Elastic Beanstalk" = "true"
    "Terraform" = "true"
  }
}

resource "aws_security_group" "eb_v2_sec_group" {
  name = "eb-sec-grp1-v2"
  description = "Elastic Beanstalk Security Group for Spring"
  vpc_id = aws_default_vpc.eb_v2_vpc.id
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 8080
      to_port = 8080
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 8
      to_port = 0
      protocol = "icmp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 5000
      to_port = 5000
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

 tags = {
    "Elastic Beanstalk" = "true"
    "Terraform" = "true"
  }
}

#Backend - Java 8 Spring 


resource "aws_elastic_beanstalk_application" "eb_v2_app_backend" {
  name = "eb-app-tf-v2-backend"
  description = "Elastic Beanstalk Application Terraform Backend"
}

resource "aws_elastic_beanstalk_environment" "eb_v2_env_backend" {
  name = "eb-env-tf-v2-backend"
  application = aws_elastic_beanstalk_application.eb_v2_app_backend.name
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.10.10 running Java 8"

  setting {
    namespace = "aws:ec2:vpc"
    name = "VPCId"
    value = aws_default_vpc.eb_v2_vpc.id
  }

  setting {
    namespace = "aws:ec2:vpc"
    name = "Subnets"
    value = aws_default_subnet.eb_v2_subnet.id
  }

  setting {
    namespace = "aws:elb:listener"
    name = "ListenerEnabled"
    value = "True"
  }

  setting {
    namespace = "aws:elb:loadbalancer"
    name = "SecurityGroups"
    value = aws_security_group.eb_v2_sec_group.id
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "ListenerProtocol"
    value = "TCP"
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "InstancePort"
    value = 80
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "InstanceProtocol"
    value = "TCP"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "Availability Zones"
    value = "Any"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MinSize"
    value = 1
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MaxSize"
    value = 2
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = "aws-elasticbeanstalk-ec2-role-v2"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "InstanceType"
    value = "t2.micro"
  }

}

#Frontend - Toamcat with Java 8

resource "aws_elastic_beanstalk_application" "eb_v2_app_frontend" {
  name = "eb-app-tf-v2-frontend"
  description = "Elastic Beanstalk Application Terraform Frontend"
}

resource "aws_elastic_beanstalk_environment" "eb_v2_env_frontend" {
  name = "eb-env-tf-v2-frontend"
  application = aws_elastic_beanstalk_application.eb_v2_app_frontend.name
  solution_stack_name = "64bit Amazon Linux 2018.03 v3.3.9 running Tomcat 8.5 Java 8"

  setting {
    namespace = "aws:ec2:vpc"
    name = "VPCId"
    value = aws_default_vpc.eb_v2_vpc.id
  }

  setting {
    namespace = "aws:ec2:vpc"
    name = "Subnets"
    value = aws_default_subnet.eb_v2_subnet.id
  }

  setting {
    namespace = "aws:elb:listener"
    name = "ListenerEnabled"
    value = "True"
  }

  setting {
    namespace = "aws:elb:loadbalancer"
    name = "SecurityGroups"
    value = aws_security_group.eb_v2_sec_group.id
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "ListenerProtocol"
    value = "TCP"
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "InstancePort"
    value = 80
  }

  setting {
    namespace = "aws:elb:listener:80"
    name = "InstanceProtocol"
    value = "TCP"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "Availability Zones"
    value = "Any"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MinSize"
    value = 1
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name = "MaxSize"
    value = 2
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = "aws-elasticbeanstalk-ec2-role-v2"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "InstanceType"
    value = "t2.micro"
  }

}

