variable "vars_image_id" {
  type = string
}
variable "vars_instance_type" {
  type = string
}
variable "vars_desired_capacity" {
  type = number
}
variable "vars_max_size" {
  type = number
}
variable "vars_min_size" {
  type = number
}
variable "subnets" {
  type = list (string)
}
variable "security_groups" {
  type = list (string)
}
variable "mod_prod" {
  type = string
}

