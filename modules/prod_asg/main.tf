

resource "aws_elb" "mod_elb_classic_aws" {
  name = "${var.mod_prod}-elb-classic"
  subnets = var.subnets
  security_groups = var.security_groups
  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
  listener {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 8080
    lb_protocol = "http"
  }
  tags = {
    "Terraform" = "true"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 10
  }

}

resource "aws_launch_template" "mod_asg_template" {
  name_prefix = "${var.mod_prod}-asg-terraform"
  image_id = var.vars_image_id
  instance_type = var.vars_instance_type
  vpc_security_group_ids = var.security_groups
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_autoscaling_group" "mod_asg_aws" {
  availability_zones = ["us-east-2a"]
  desired_capacity = var.vars_desired_capacity
  max_size = var.vars_max_size
  min_size = var.vars_min_size
  launch_template {
    id = aws_launch_template.mod_asg_template.id
    version = "$Latest"
  }

  tag {
    key = "Terraform"
    value = "true"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "mod_asg_attachment_elb" {
  autoscaling_group_name = aws_autoscaling_group.mod_asg_aws.id
  elb = aws_elb.mod_elb_classic_aws.id
}
