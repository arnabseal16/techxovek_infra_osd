variable "vars_image_id" {
  type = string
}
variable "vars_instance_type" {
  type = string
}
variable "vars_cidr_blocks" {
  type = list(string)
}
variable "vars_desired_capacity" {
  type = number
}
variable "vars_max_size" {
  type = number
}
variable "vars_min_size" {
  type = number
}


provider "aws" {
  profile = "default"
  region = "us-east-2"
}

resource "aws_default_vpc" "prod_default_vpc_aws" {}

resource "aws_default_subnet" "prod_default_subnet_az1_aws" {
  availability_zone = "us-east-2a"
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_security_group" "prod_secgrp_web" {
  name = "terraform_web_az2"
  description = "allow standard https/http"
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
    from_port = 8
    to_port = 0
    protocol = "icmp"
    cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = var.vars_cidr_blocks
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = var.vars_cidr_blocks
  }
  tags = {
    "Terraform" = "true"
  }
}
module "mod_prod" {
  source = "./modules/prod_asg"

  vars_image_id = var.vars_image_id
  vars_instance_type = var.vars_instance_type
  vars_desired_capacity = var.vars_desired_capacity
  vars_max_size = var.vars_max_size
  vars_min_size = var.vars_min_size
  subnets = [aws_default_subnet.prod_default_subnet_az1_aws.id]
  security_groups = [aws_security_group.prod_secgrp_web.id]
  mod_prod = "modular-prod"
}

