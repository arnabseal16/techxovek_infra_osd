variable "vars_image_id" {
  type = string
}
variable "vars_instance_type" {
  type = string
}
variable "vars_cidr_blocks" {
  type = list(string)
}
variable "vars_desired_capacity" {
  type = number
}
variable "vars_max_size" {
  type = number
}
variable "vars_min_size" {
  type = number
}


provider "aws" {
  profile = "default"
  region = "us-east-2"
}

resource "aws_default_vpc" "prod_default_vpc_aws" {}

resource "aws_default_subnet" "prod_default_subnet_az1_aws" {
  availability_zone = "us-east-2a"
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_security_group" "prod_secgrp_web" {
  name = "terraform_web_az2"
  description = "allow standard https/http"
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
    from_port = 8
    to_port = 0
    protocol = "icmp"
    cidr_blocks = var.vars_cidr_blocks
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = var.vars_cidr_blocks
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = var.vars_cidr_blocks
  }
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_elb" "prod_elb_classic_aws" {
  name = "prod-elb-classic"
  subnets = [aws_default_subnet.prod_default_subnet_az1_aws.id]
  security_groups = [aws_security_group.prod_secgrp_web.id]
  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
  listener {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 8080
    lb_protocol = "http"
  }
  tags = {
    "Terraform" = "true"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 10
  }

}

resource "aws_launch_template" "prod_asg_template" {
  name_prefix = "prod-asg-terraform"
  image_id = var.vars_image_id
  instance_type = var.vars_instance_type
  vpc_security_group_ids = [aws_security_group.prod_secgrp_web.id]
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_autoscaling_group" "prod_asg_aws" {
  availability_zones = ["us-east-2a"]
  desired_capacity = var.vars_desired_capacity
  max_size = var.vars_max_size
  min_size = var.vars_min_size
  launch_template {
    id = aws_launch_template.prod_asg_template.id
    version = "$Latest"
  }

  tag {
    key = "Terraform"
    value = "true"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "prod_asg_attachment_elb" {
  autoscaling_group_name = aws_autoscaling_group.prod_asg_aws.id
  elb = aws_elb.prod_elb_classic_aws.id
}
