provider "aws" {
  profile = "default"
  region = "us-west-2"
}

resource "aws_s3_bucket" "prod_s3_bucket" {
  bucket = "prod-s3-defvpc-tf-aws-arnabseal16"
  acl = "private"
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_default_vpc" "prod_default_vpc_aws" {}

resource "aws_default_subnet" "prod_default_subnet_az1_aws" {
  availability_zone = "us-west-2a"
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_default_subnet" "prod_default_subnet_az2_aws" {
  availability_zone = "us-west-2b"
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_security_group" "prod_secgrp_web" {
  name = "terraform_web"
  description = "allow standard https/http"
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_instance" "prod_nginx_ec2_aws_v1" {
  count = 2
  ami = "ami-03bc0cadd4e79fcbe"
  instance_type = "t2.nano"
  vpc_security_group_ids = [aws_security_group.prod_secgrp_web.id]
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_eip_association" "prod_eip_assoc_aws_v1" {
  instance_id = aws_instance.prod_nginx_ec2_aws_v1[0].id
  allocation_id = aws_eip.prod_ec2_eip_aws_v1.id
}

resource "aws_eip" "prod_ec2_eip_aws_v1" {
  instance = aws_instance.prod_nginx_ec2_aws_v1[0].id
  tags = {
    "Terraform" = "true"
  }
}

resource "aws_elb" "prod_elb_classic_aws" {
  name = "prod-elb-classic"
  instances = aws_instance.prod_nginx_ec2_aws_v1[*].id
  subnets = [aws_default_subnet.prod_default_subnet_az1_aws.id, aws_default_subnet.prod_default_subnet_az2_aws.id]
  security_groups = [aws_security_group.prod_secgrp_web.id]
  listener {
    instance_port = 80
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }
  tags = {
    "Terraform" = "true"
  }
}